<!--
SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# My Vim Configuration

Hello and welcome to my custom configuration for Vim! This configuration is
tailored for my needs. Add or remove whatever you want, just understand that
this configuration is very hands on.

You may notice that this configuration uses git submodules for plugin
management. Why? because this configuration is tightly linked to my rice setup.
Any changes made to the rice leads to changes in this configuration, so
managing dependencies through git is easier and more flexible.

## Install

Just clone and run `make install`. View the [installation file][install-file]
for more details about installing my configuration.

I recommend you cherry pick what you want into your configuration rather than
just copying it. Understand that this configuration was made for me, and not
for the public at large. Therefore, things may break or seem weird if you do
not understand the purpose of it outright.

## Configurations

The [leader][mapleader] is a comma `,`; so, whenever you see `<leader>` it means
to press comma on your keyboard.

### Normal Mode Mappings

| Keys             | Function                                            |
| ---------------- | --------------------------------------------------- |
| \<space\>        | Search                                              |
| \<C-space\>      | Backwards search                                    |
| \<leader\>\<cr\> | Disable highlights                                  |
| \<leader\>w      | Quick save                                          |
| :W               | Save as sudoer                                      |
| \<C-j\>          | Move down a window                                  |
| \<C-k\>          | Move up a window                                    |
| \<C-h\>          | Move left a window                                  |
| \<C-l\>          | Move right a window                                 |
| \<leader\>hs     | Create horizontal split                             |
| \<leader\>vs     | Create vertical split                               |
| \<leader\>bo     | Open new buffer                                     |
| \<leader\>bd     | Close current buffer                                |
| \<leader\>ba     | Close all buffers                                   |
| \<leader\>bh     | Move to previous buffer                             |
| \<leader\>bl     | Move to next buffer                                 |
| \<leader\>tn     | Open new tab                                        |
| \<leader\>to     | Close all other tabs (keeping current tab open)     |
| \<leader\>th     | Move to previous tab                                |
| \<leader\>tl     | Move to next tab                                    |
| \<leader\>tm     | Move a tab to a new position                        |
| \<leader\>te     | Open new tab with current buffer's path             |
| \<leader\>cd     | Change directory to the directory of current buffer |
| \<leader\>pm     | Toggle paste mode                                   |

### Visual Mode Mappings

| Keys  | Function                           |
| ----- | ---------------------------------- |
| *     | Search current selection           |
| #     | Backwards search current selection |

### Insert Mode Mappings

| Keys  | Function          |
| ----- | ----------------- |
| jj    | Enter normal mode |

## Contributing

My Vim configuration is opinionated, but not subborn. This project is open to
the following forms of contribution:

- Bug reports and fixes.
- Feature requests and implementations.
- Improvments to documentation.

View the [contributing file][contributing-file] for some guidelines about
contribution. Any help or advice will be appreciated regardless.

## License

View the [license file][license-file] or view the contents of the
[LICENSES][license-dir] directory for the legal stuff.

-------------------------------------------------------------------------------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0"
   src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"/>
</a><br/>
This document is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  Creative Commons Attribution-ShareAlike 4.0 International License
</a>.

[install-file]: https://gitlab.com/rice-config/my-vim-config/blob/main/INSTALL.md
[contributing-file]: https://gitlab.com/rice-config/my-vim-config/blob/main/CONTRIBUTING.md
[license-file]: https://gitlab.com/rice-config/my-vim-config/blob/main/LICENSE.md
[mapleader]: https://learnvimscriptthehardway.stevelosh.com/chapters/06.html#leader
[license-dir]: https://gitlab.com/rice-config/my-vim-config/blob/main/LICENSES

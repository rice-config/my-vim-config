" SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
" SPDX-License-Identifier: ISC

" General settings

" Enable fileype plugins...
filetype plugin on
filetype indent on

" Set to auto read when a file is modified from the outside...
set autoread
au FocusGained,BufEnter * checktime

" Set history size and cursor lines...
set history=1000
set so=7

" Establish default file settings...
let $LANG = 'en'
set langmenu=en
set encoding=utf8
set ffs=unix,dos,mac
set nobackup
set nowb
set noswapfile

" Environment variables...
set viminfo+=n$XDG_STATE_HOME/vim/viminfo
set runtimepath=$XDG_CONFIG_HOME/vim,$XDG_CONFIG_HOME/vim/after,$VIM,$VIMRUNTIME
set packpath=$XDG_CONFIG_HOME/vim,$XDG_CONFIG_HOME/vim/after,$VIM,$VIMRUNTIME
let g:netrw_home="$XDG_CACHE_HOME/vim"
let $MYVIMRC="$XDG_CONFIG_HOME/vim/vimrc"

" Set command-line height...
set cmdheight=1

" Enable wild menu...
set wildmenu

" Make backspace more natural...
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Smart searching with casing...
set smartcase

" Highlight searches...
set hlsearch

" More natural searching...
set incsearch

" Do not redraw when executing macros...
set lazyredraw

" Turn on regular expressions...
set magic

" Bracket settings...
set showmatch
set mat=3

" Remove annoying error sounds...
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Enable syntax highlighting...
syntax enable

" Setup default theme...
colorscheme dracula
let g:airline_theme='dracula'

" Hide abandoned buffers...
set hid

" Specify behavior when switching between buffers...
try
  set switchbuf=useopen,usetab,newtab
  set stal=2
catch
endtry

" Make splits more natural...
set splitbelow
set splitright

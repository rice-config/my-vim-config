" SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
" SPDX-License-Identifier: ISC

" Keyboard mappings...

" Setup leader...
let mapleader = ","

" Normal mode mappings...

" Remap Vim 0 to first non-blank character...
map 0 ^

" Forward search...
map <space> /

" Backwards search...
map <C-space> ?

" Fast save...
nmap <leader>w :w!<cr>

" Save as sudo...
command! W execute 'w !sudo tee % > /dev/null' <bar> edit!

" Window movement...
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Split mappings...
map <leader>hs :split
map <leader>vs :vsplit

" Buffer mappings...
map <leader>bo :enew<cr>
map <leader>bd :Bclose<cr>:tabclose<cr>gT
map <leader>ba :bufdo bd<cr>
map <leader>bl :bnext<cr>
map <leader>bh :bprevious<cr>

" Tab mappings...
map <leader>tn :tabnew
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove
map <leader>th :tabprev<cr>
map <leader>tl :tabnext<cr>
map <leader>te :tabedit <C-r>=expand("%:p:h")<cr>/
map <leader>cd :cd %:p:h<cr>:pwd<cr>

" Toggle paste mode...
map <leader>pp :setlocal paste!<cr>

" Visual mode mappings...

" Forward search...
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>

" Backwards search...
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

" Helper for visual selection...
fun! VisualSelection(direction, extra_filter) range
  let l:save_reg = @"
  execute "normal! vgvy"

  let l:pattern = escape(@", "\\/.*'$^~[]")
  let l:pattern = substitute(l:pattern, "\n$", "", "")

  if a:direction == 'gv'
    call CmdLine("Ack '" . l:patern . "' " )
  elseif a:direction == 'replace'
    call CmdLine("%s" . '/'. l:pattern . '/')
  endif

  let @/ = l:pattern
  let @" = l:saved_reg
endfun

" Insert mode mappings...

" Alternate way to escape...
imap jj <Esc>

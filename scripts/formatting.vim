" SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
" SPDX-License-Identifier: ISC

" Formatting settings.

" Set line break...
set lbr

" Set maximum column limit to 100...
set tw=100

" Use spaces instead of tabs...
set expandtab

" Smart tabbing...
set smarttab

" Enable hard wrap...
set wrap

" 1 tab = 4 spaces...
set shiftwidth=4
set tabstop=4
set softtabstop=4

" Auto indent...
set ai

" Smart indent...
set si

" Return to last edit position when opening files...
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Delete trailing white space on save, useful for some filetypes...
fun! CleanExtraSpaces()
  let save_cursor = getpos(".")
  let old_query = getreg('/')
  silent! %s/\s\+$//e
  call setpos('.', save_cursor)
  call setreg('/', old_query)
endfun

if has("autocmd")
  autocmd BufWritePre *.txt,*.md,*.vim,*.c,*.cpp,*.h,*.hpp,*.py,*.sh,*.zsh :call CleanExtraSpaces()
endif

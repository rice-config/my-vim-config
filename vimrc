" SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
" SPDX-License-Identifier: ISC

so $XDG_CONFIG_HOME/vim/scripts/general.vim
so $XDG_CONFIG_HOME/vim/scripts/formatting.vim
so $XDG_CONFIG_HOME/vim/scripts/mappings.vim

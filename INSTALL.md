<!--
SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Installing my Vim configuration

The following information provides details for properly installing my Vim
configuration.

> __NOTE:__ I do not recommend that you blindly install without viewing the code
> first. In case something breaks, you will have a rough idea about where and
> what to fix.

## Getting valid sources

You can get valid sources of my configuration from my github account at
\<<https://gitlab.com/rice-config/my-vim-config>\>. I recommend that you clone
this repository as a git submodule.

## Checking signitures

My configuration uses [semantic versioning][semver-website] via git tags. These
tags are cryptographically signed by my GPG key \<<xetikus@gmail.com>\>. You
can get my public key at \<<https://keys.openpgp.org>\>. 

Add the following snippet into your `gpg.conf` file:

```
keyserver hkps://keys.openpgp.org
```

Now type the following to retrieve my public key:

```
gpg --auto-key-locate keyserver --locate-keys xetikus@gmail.com
```

I recommend that you trust the key up to at least level 4. The subkeys expire
every four months, while the master key itself expires in one year. I of course
maintain this key, so you can refresh your copy using this command:

```
gpg --refresh-keys
```

To verify the signitures of git tags simply type the following:

```
git verify-tag $TAG
```

## Installing

Every aspect of this configuration is managed by a simple Makefile just run
`make install` to setup everything up. You can also run `make update` to update
the submodules. You can also do everything manually by moving this configuration
into the `.config/vim` directory.

-------------------------------------------------------------------------------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0"
   src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"/>
</a><br/>
This document is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  Creative Commons Attribution-ShareAlike 4.0 International License
</a>.

[semver-website]: https://semver.org

<!--
SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Author of this Vim configuration

Written and maintained by Jason Pena \<<xetikus@gmail.com>\>.

-------------------------------------------------------------------------------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0"
   src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"/>
</a><br/>
This document is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  Creative Commons Attribution-ShareAlike 4.0 International License
</a>.

<!--
SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Need to do

- [X] Fix vim pack packge loading.
- [X] setup Git Gutter plugin
- [X] Setup fugative plugin.
- [X] Setup vim-airline plugin.
- [X] Setup NERD tree plugin.
- [X] Setup syntastic plugin.
- [X] Setup dracula theme.

-------------------------------------------------------------------------------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0"
   src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"/>
</a><br/>
This document is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  Creative Commons Attribution-ShareAlike 4.0 International License
</a>.

<!--
SPDX-FileCopyrightText: 2022 Jason Pena <xetikus@gmail.com>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# License Information

My Vim configuration uses the [ISC license][isc-copy] for source code, and the
[CC-BY-SA 4.0 license][cc-by-sa-4.0-copy] for documentation. So long as you
abide by these licenses you can freely copy and redistribute this repository
across the internet!

[isc-copy]: https://github.com/rice-config/my-vim-config/blob/main/LICENSES/ISC.txt
[cc-by-sa-4.0-copy]: https://github.com/rice-config/my-vim-config/blob/main/LICENSES/CC-BY-SA-4.0.txt
